'use strict';

var model = require('mysql-easy-model').model;

var User = model('vaga', {
	table: 'vaga',
	fields: ['id_vaga', 'nome', 'descricao', 'email', 'id_especialidade', 'id_cidade'],
	primary: ['id_vaga']
});