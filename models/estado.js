'use strict';

var model = require('mysql-easy-model').model;

var User = model('estado', {
	table: 'estado',
	fields: ['id_estado', 'sigla', 'nome_estado'],
	primary: ['id_estado']
});