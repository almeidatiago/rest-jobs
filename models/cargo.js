'use strict';

var model = require('mysql-easy-model').model;

var User = model('cargo', {
	table: 'cargo',
	fields: ['id_cargo', 'nome_cargo', 'id_especialidade'],
	primary: ['id_cargo']
});