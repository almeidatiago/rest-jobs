'use strict';

var express  = require('express');
var path     = require('path');
var bodyParser = require('body-parser');
var fs = require('fs');

var mysqlEasyModel = require('mysql-easy-model');

mysqlEasyModel.createConnection({
    connectionLimit : 10,
    host            : 'localhost',
    user            : 'root',
    password        : '256512700',
    database        : 'jobs'
});

//bootstrap models
fs.readdirSync(path.join(__dirname, '/models')).forEach(function (file) {
  if (~file.indexOf('.js')) require(__dirname + '/models/' + file);
});

var vaga = require('./routes/vaga')
var app = express();

app.use(bodyParser.urlencoded({ extended: true })); //support x-www-form-urlencoded
app.use(bodyParser.json());

app.get('/vaga/', vaga.findAll);
app.get('/vaga/:id', vaga.findOne);
app.get('/vaga/:idCidade/:idEspecialidade', vaga.findByCargoAndEstado);


//start Server
var server = app.listen(3000,function(){

   console.log("Listening to port %s",server.address().port);

});
