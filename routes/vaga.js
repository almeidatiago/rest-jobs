'use strict';

var Vaga = require('mysql-easy-model').model('vaga');

exports.findAll = function(req, res, next){
    Vaga.find(function(err, vagas){
        if(!err) res.status(200).json(vagas);
        else next(err);
    });
};

exports.findOne = function(req, res, next){
    var id = req.params.id;
    Vaga.find({id_vaga: id}, function(err, vagas){
        if(!err) res.status(200).json(vagas);
        else next(err);
    });
};

exports.findByCargoAndEstado = function(req, res, next){
	var idCidade = req.params.idCidade;
    var idEspecialidade = req.params.idEspecialidade;

	var query = 'select v.id_vaga, v.nome_vaga, c.nome_cidade, e.nome_estado, es.nome_especialidade from vaga v inner join cidade c on v.id_cidade = c.id_cidade inner join estado e on c.id_estado = e.id_estado inner join especialidade es on v.id_especialidade = es.id_especialidade	where v.id_cidade = ? and v.id_especialidade = ?'

    Vaga.query(query, [idCidade, idEspecialidade], function(err, vagas){
    	if(!err) res.status(200).json(vagas);
    	else next(err);
	});
};